import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Input } from '@angular/core';
import { ProgsProviderFireApi } from '../../providers/progs/progs';
import { ViewController, AlertController, LoadingController, NavController } from 'ionic-angular';

//Page
import { HomePage } from '../../pages/home/home';

// For Modal
import { ModalController } from 'ionic-angular';

//Native
import { CallNumber } from '@ionic-native/call-number';
import { Network } from '@ionic-native/network';

/**
 * Generated class for the ResultatListPartialComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'resultat-list-partial',
  templateUrl: 'resultat-list-partial.html'
})
export class ResultatListPartialComponent implements OnInit {
  @Input() itemIncomes: any; //Réception des données entrer par l'utilisateur dans le filtre
  @Input() arrayProduitsFromRequeteFiltre: any; //Réception de tous les produits from API REST
  @Output() sendNbreResultToParent = new EventEmitter<number>();
 
  arrayResult: any[]=[]; //Tableau des resultats à afficher après filtre selon CRITERES

  cacheImg = []; // garde une seule image
  cacheAllImg = []; // garde toutes les images récupérer from REST
  cacheImgResult : any[]=[];
  numberResult : number;
  urlProduits: string;
  loaderSearch : any;
  numeroSablux = "338696030"; //Numéro SABLUX
  loader: any; //Loader

  //--------------------------------------------------METHODES LIFECYCLE----------------------------------------
  constructor(public navCtrl: NavController, private alertCtrl: AlertController, public loadingCtrl: LoadingController, private network: Network, public alertController: AlertController, public callNumber: CallNumber, private progsServiceFireApi: ProgsProviderFireApi, public modalCtrl : ModalController) {}
  
  ngOnInit(): void {
    this.traitementProduits();
  }

  //------------------------------------METHODES LOGIQUE METIERS----------------------------------------------
  //Methodes boutons
  public openContacter(produit : any) {
    var data = { typeContact : 'contactFromproduit', produitAll : produit};
    var modalPage = this.modalCtrl.create('page-contact', data); 
    modalPage.present(); 
  }
  public openAppeler(numero : string) {
    this.callNumber.callNumber(numero, true)
      .then(() => console.log('Phone ouvert'))
      .catch(() => {
        this.alertMessage('Impossible d\'utiliser le phone' , "L\'application n'a pas réussi à ouvrir le phone");
      });  
  }

  //Alerte
  alertMessage(title, detail) {
    let alert = this.alertController.create({
      title: title.toString(),
      subTitle: detail.toString(),
      buttons: ['Fermer']
    });
    alert.present();
  }

  //For Api REST
  traitementProduits() { //   || (( this.itemIncomes.zone == "none") && (this.itemIncomes.type == "none") && (this.itemIncomes.prix == 0))  Set un tableau avec les résultat et renvoi le nbre de résultat.
      //---------------------------------FILTRAAAGE---------------------------------
      this.loaderSearch = this.loadingCtrl.create({
        content: "Recherche en cours..."
      });
      this.loaderSearch.present();
      if(( this.itemIncomes.zone == "none") && (this.itemIncomes.type == "none") && (+this.itemIncomes.prix == 0))
      {
        //Cas ALL none
        //this.alertMessage("","Veuillez choisir une zone ou un type"); //pour ce cas cé rapide il réagit immédiatement. Cool. ok ok je vois. on etabli un prot
        //this.arrayResult = this.arrayProduitsFromRequeteFiltre;
        console.log('aucun résultat');

      }else if((( this.itemIncomes.zone != "none") && (this.itemIncomes.type == "none") && (+this.itemIncomes.prix == 0) ) || (( this.itemIncomes.zone == "none") && (this.itemIncomes.type != "none") && (+this.itemIncomes.prix == 0)) || (( this.itemIncomes.zone == "none") && (this.itemIncomes.type == "none") && (+this.itemIncomes.prix != 0)) ) {
        //Cas ou une seule valeur est donnée (que ce soit zone, type ou prix)
        for (let item of this.arrayProduitsFromRequeteFiltre) {
          if((item.array_options.options_zone == this.itemIncomes.zone) || (item.array_options.options_type == this.itemIncomes.type) || (item.price <= +this.itemIncomes.prix)) {
            this.arrayResult.push(item);
          }else{
            //Cas ou le seul critère ne match pas avec API
            console.log('Cas 1: item don\'t match '+item.array_options.options_zone+'  Type produit  '+item.array_options.options_type+' PRIX  '+item.price)          
          }
        }
      }else if((( this.itemIncomes.zone != "none") && (this.itemIncomes.type != "none") && (+this.itemIncomes.prix == 0) ) || (( this.itemIncomes.zone != "none") && (this.itemIncomes.type == "none") && (+this.itemIncomes.prix != 0)) || (( this.itemIncomes.zone == "none") && (this.itemIncomes.type != "none") && (+this.itemIncomes.prix != 0)) ) {
        //Cas ou 2 critères sont données 
        for (let item of this.arrayProduitsFromRequeteFiltre) {
          if(((item.array_options.options_zone == this.itemIncomes.zone) && (item.array_options.options_type == this.itemIncomes.type)) || ((item.array_options.options_zone == this.itemIncomes.zone) && (item.price <= +this.itemIncomes.prix)) || ((item.array_options.options_type == this.itemIncomes.type) && (item.price <= +this.itemIncomes.prix))  ){      
            this.arrayResult.push(item);
          }else{
            //Cas ou les 2 critères ne match pas avec API
            console.log('Cas 2: item don\'t match '+item.array_options.options_zone+'  Type produit  '+item.array_options.options_type+' PRIX  '+item.price)
          }
        }
      }else {
        //Cas ou tous les critères sont renseignés.
        for (let item of this.arrayProduitsFromRequeteFiltre) {
          if((item.array_options.options_zone == this.itemIncomes.zone) && (item.array_options.options_type == this.itemIncomes.type) && (item.price <= +this.itemIncomes.prix)) {//ZONE

            this.arrayResult.push(item);
            console.log("GOOD ZONE:  "+item.array_options.options_zone+"  TYPE : "+item.array_options.options_type+" etage "+item.niveauetage+" et qui coute "+item.price);

          }else {
            //Cas ou les 3 critères ne match pas avec API
            console.log('Cas 3: item don\'t match '+item.array_options.options_zone+'  Type produit  '+item.array_options.options_type+' PRIX  '+item.price)
          } //If ZONE
        }
      }
      this.loaderSearch.dismiss();
      this.numberResult = this.arrayResult.length;
      this.sendNbreResultToParent.emit(this.numberResult);// On emet le nombre de resultat
      //console.log("Voici le tableau : "+this.arrayResult);
      //------------------------FINNNN---------FILTRAAAGE---------------------------------
  }

  //Methodes pour tester la connection 
    //Method that returns true if the user is connected to internet
    private isOnline(): boolean {
      return this.network.type.toLowerCase() !== 'none';
    }
    // Method that returns true if the user is not connected to internet
    private isOffline(): boolean {
      return this.network.type.toLowerCase() === 'none';
    }

  //Methode Nbre de résultat
    countResult() : number{
      return this.arrayProduitsFromRequeteFiltre;
    }

  // Methode qui recupère les images d'un bien
  private getImgBienService (idbien : number) {
    //déclaration variables
    let urlImgIdBien : string = 'http://196.207.207.64/service/api/index.php/clients/photos/'+idbien+'?';
    //Recupération Img produits from API REST  (utilisation du service progs.ts) 
    this.progsServiceFireApi.restGet(urlImgIdBien).then (data => {
      this.cacheImg = data;
    })
  } 

  //Méthode qui récupère toutes les images des biens de services et les met dans cacheAllImg. MEc c'est impossible. sepro n'a pas mis d'id sur les phto donc si on les regroupe on saura pas quels photos appartient a quel bien??. tu as raison dé javais pa pensé à ça c'est vrai qu c'est l'id du bien qu'on a pas celui de la ohoto.donc on peut recrée nous meme une autre structure pour garder ca. je reflechi je te reviens noww. hum je vois ok.
  private getAllImgBienService (){
    for (let item of this.arrayResult) {
      this.cacheAllImg.push(this.getImgBienService(item.id))
    }
  }
}



//-----------SNIPPETS------------------
//console.log("Un produit :  "+item.zone+"  ID: "+item.id+" de type "+item.typeproduit+" et qui coute "+item.price);
//this.arrayProduitsFromRequeteFiltre.indexOf(item.zone) <= -1 

  //METHODES LOGIQUE METIER




//----------------SNIPPPETS----------------------
// for (let item of this.arrayProduitsFromRequeteFiltre) {
        //   if((item.zone == this.incomes.zone) && (item.type == this.incomes.type) && (item.etage == this.incomes.etage) && (item.prix <= this.incomes.prix)) {//ZONE

        //     this.arrayResult.push(item);
        //     console.log("GOOD ZONE:  "+item.zone+"  TYPE : "+item.typeproduit+" etage "+item.niveauetage+" et qui coute "+item.price);

        //   }else if((item.zone == this.incomes.zone) && (item.type != this.incomes.type) && (item.etage != this.incomes.etage) && (item.prix >= this.incomes.prix)){
        //     console.log("Item ne respecte pas les critères");
        //   } // If ZONE
        // }
//console.log("Un produit :  "+item.zone+"  ID: "+item.id+" de type "+item.typeproduit+" et qui coute "+item.price);
//this.arrayProduitsFromRequeteFiltre.indexOf(item.zone) <= -1 

  //METHODES LOGIQUE METIER




//----------------SNIPPPETS----------------------
// for (let item of this.arrayProduitsFromRequeteFiltre) {
        //   if((item.zone == this.incomes.zone) && (item.type == this.incomes.type) && (item.etage == this.incomes.etage) && (item.prix <= this.incomes.prix)) {//ZONE

        //     this.arrayResult.push(item);
        //     console.log("GOOD ZONE:  "+item.zone+"  TYPE : "+item.typeproduit+" etage "+item.niveauetage+" et qui coute "+item.price);

        //   }else if((item.zone == this.incomes.zone) && (item.type != this.incomes.type) && (item.etage != this.incomes.etage) && (item.prix >= this.incomes.prix)){
        //     console.log("Item ne respecte pas les critères");
        //   } // If ZONE
        // }


    
        //this.incomes.etage = this.navParams.get('etage');
    //this.incomesAP = JSON.parse(this.incomes);
    //console.log("Voici la zone : "+this.navParams.get('zone'));




    //Si le phone est déconnecté
    // if(this.isOnline()) { 
    //   //ToDO: Si le phone est online
    //   this.presentLoading();//Affiche le loading
    //   this.restGetProduits();
    // } else {
    //   let alertDecon = this.alertCtrl.create({
    //     title: "Déconnecté !!",
    //     subTitle: "Veuillez vous connecter pour accéder aux services",
    //     buttons: [{
    //       text: 'Retour à l\'accueil',
    //       handler: () => {
    //         //TODO quand on clique une notif LOCAL
    //         this.navCtrl.setRoot(HomePage);
    //       }
    //     }]
    //   });
    //   alertDecon.present();
    // }
  