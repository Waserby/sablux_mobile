import { Component, Input, OnInit } from '@angular/core';
import { ProgsProviderFireApi } from '../../providers/progs/progs';
import { NavController, AlertController, Platform, LoadingController } from 'ionic-angular';

//Native
import { LocalNotifications } from '@ionic-native/local-notifications';
import { Storage as Store } from '@ionic/storage';
import { HomePage } from '../../pages/home/home';
import { Subscription } from 'rxjs';

/**
 * Generated class for the FiltrePartialComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'filtre-partial',
  templateUrl: 'filtre-partial.html'
})
export class FiltrePartialComponent implements OnInit {
  @Input() itemStatut: String; // recoit le statut (Louer/Vendre) From Component Parent qui est Trouver-bien
  tabProduits: any;//Tableau de programme LOCAL: string;
  urlProduits: string;
  token : string;
  arrayZone: string[] = []; //Tableau des zone disponible dans les programmes de l'API
  //NG MODEL des inputs
  inputs = { zone: "none", type: "none", prix: 0 };// On initialise pour les default entrie et on recupère en two way databinding
  text: number = 0;//Pour filtre template
  boutonRechercherOn : boolean;
  loader: any; //Loader
  //Les evenements declenchés à l'ouverture de cette page
  eventConnect : Subscription;
  eventDisconnect : Subscription;
  eventNotifFiltre : Subscription;

  //------------------------------------------METHODES LIFECYCLE--------------------------------------------
  constructor(public loadingCtrl: LoadingController, private plt: Platform, private alertCtrl: AlertController, public storage: Store, private localNotifications: LocalNotifications, public navCtrl: NavController, private progsServiceFireApi: ProgsProviderFireApi) {}//FIN contructeur

  ngOnInit(): void {
    //New url SERVICE: Update 29/11/18 ---------- http://seproerp.ddns.net:8080/service/api/index.php/products?api_key=token
    this.urlProduits = 'http://196.207.207.64/service/api/index.php/products?'; //Without token (j ajoute le bon token dans le service)
    this.boutonRechercherOn = true;

    //Quand on clique sur une notife: TRaitement suivant
    this.plt.ready().then(() => {
      //TODO quand on clique une notif
      this.eventNotifFiltre = this.localNotifications.on('click').subscribe( notification => {
        //Ca lance une alerte avec un bouton menant vers la notification
        let alert = this.alertCtrl.create({
          title: notification.title,
          subTitle: notification.text,
          buttons: [{
            text: 'Voir',
            handler: () => {
              //TODO quand on clique une notif LOCAL
              this.sendInputs({zone: notification.data.array_options.options_zone, type: this.typeNumberToString(notification.data.array_options.options_type), prix: notification.data.price});       
            }
          }]
        });
        alert.present();
      })
    })//PLATFORM READY


    // //TEST
    // this.storage.set('stockerBool', true);// Je met la variable à true si les produits ont été recuperer
    // this.storage.get('produits').then(data => {console.log('MON ARRAY SORTTTT'+data)});
    // this.arrayZone = this.arrayZone.sort();
    // this.storage.set('arrayZone', this.arrayZone.sort());// Je stocke le nouveau tableau de zones
    // console.log('MON ARRAY SORTTTT'+JSON.stringify(this.arrayZone.sort()));
    // this.storage.get('arrayZone').then(data => {console.log(data)});
    
    //Algo pour executer le loading from API une seule fois par jour ()
    this.storage.get('stockerBool').then(bool => {

      console.log("Boolean stock test: "+bool);
      if (bool == null || bool == false) {//Quand le user viens d'entrer dans l'appli (bool = nul) ou que le bool est a false cad on doit recharger
        
        this.presentLoading();
        this.restGetProduits();
      
      }else {//Tableau de produits déja chargé donc on ne recharge pas, on use ce qui est stocker
        
        this.storage.get('produits').then(data => { this.tabProduits = data});// Le tableau de produits précédemment stocker est recuperer
        this.storage.get('arrayZone').then(data => {this.arrayZone = data});
    
      }

    })
    .catch(err => {

      console.log('Your data don\'t exist and returns error in catch: ' + JSON.stringify(err));

    });

  }

  //---------------------------------------------METHODES LOGIQUE METIER------------------------------------------------
  //For Api REST
  restGetProduits() {

    this.progsServiceFireApi.restGet(this.urlProduits).then(data => {
      this.storage.set('stockerBool', true);// Je met la variable à true si les produits ont été recuperer
      this.tabProduits = data;
      
      //On store juste au premier lancement de l'application
      this.storage.get('produits').then(donnee => {
        if (!donnee) {
          this.storage.set('produits', this.tabProduits);// Je STORE le tableau de produits pour les notifications        
        }
      })
        .catch(err => {
          console.log('Your data don\'t exist and returns error in catch: ' + JSON.stringify(err));
        });

      this.testNewProgramme(this.tabProduits);//On test si il y'a un nouveau programme

      //Pour les ZONES
      for (let item of this.tabProduits) {
        if ((this.arrayZone.indexOf(item.array_options.options_zone) == -1) && (item.array_options.options_zone !== null) && (item.array_options.options_zone !== "")) { //this.tabProduits.indexOf(item.zone) <= -1 
          this.arrayZone.push(item.array_options.options_zone);
        }
      }
      this.arrayZone = this.arrayZone.sort();
      this.loader.dismiss();// On fait disparaitre le loader
      this.storage.set('arrayZone', this.arrayZone);// Je stocke le nouveau tableau de zones

    });

  }

  //Methode de VALIDATION DU FORMULAIRE
  sendInputs(inputs : any) {
    let envoi = {datafiltre: inputs, tabAllProduits: this.tabProduits};
    this.navCtrl.push('page-resultat-recherche', envoi);//J'envoi la valeur à travers un navCTRL
  }

  //Methode pour NOTIFIER
  notifier(titre: string, text: string, item?: any) {
    // Schedule a single notification
    this.localNotifications.schedule({
      title: titre,
      text: text,
      icon: 'assets/img/icon.png',
      data: item
    });
    //global.skipLocalNotificationReady = true;
  }

  //METHODE TEST Si il ya un NEW PROGRAMME
  testNewProgramme(nouveauTabProduits) {
    let nbreNewProd;
    this.storage.get('produits').then((result) => {
      if (nouveauTabProduits.length > result.length) {//Si le nouveau TabProduits est plus grand (>19)
        //Dans ce cas, on a un nouveau produit
        nbreNewProd = (nouveauTabProduits.length - result.length);
        console.log("Nombre de New ITEMS: " + nbreNewProd);
        //Boucle pour obtenir maxItemNumber
        let maxItemNumber: number = 0;
        for (let item of result) {
          let itemNumber = +item.id;
          console.log("ID result: " + itemNumber);
          if (itemNumber > maxItemNumber) {
            maxItemNumber = itemNumber;
          }
        }//Fin Boucle MAX item Number
        console.log("BIGGER ID : " + maxItemNumber);
        //On notifie si l'id de l'item de la nouvelle liste est supérieur au plus grand id d'item du tableau stocké
        for (let item of nouveauTabProduits) {
          let itemNumb = +item.id; //Transformer les lettres en chiffres

          if (itemNumb > maxItemNumber) {
            //Je notifie car l'item a un id supérieur à 19
            let titreNotif: string = "SABLUX: Un nouvel appartement est disponible";
            let contenuNotif: string = "C'est un " + this.typeNumberToString(item.array_options.options_type) + " situé à " + item.array_options.options_zone;
            this.notifier(titreNotif, contenuNotif, item);//On notifie
            console.log(" L'ID de l'item : " + itemNumb + " Son contenu" + item);
            //Je set encore la nouvelle liste de produits pour un prochain test
            this.storage.set('produits', this.tabProduits);// Je STORE le tableau de produits pour les notifications        
          } else {
            console.log("BAD ITEM: L'ID de l'item : "+itemNumb);
          }
        }
      } else {
        //Dans ce cas, Aucun produit n'a été ajouté
        console.log("Aucun Produit Ajouté");
        let titreNotif: string = "Aucun nouveau produit";
        let contenuNotif: string = "Il n'y a pas de nouvel appartement disponible";
        this.notifier(titreNotif, contenuNotif);//On notifie
      }
    });
  }//FIN testProgramme

  //--------------Loader Methode--------------
    //Le loading pendant que ca charge
    presentLoading() {

      this.loader = this.loadingCtrl.create({
        content: "Chargement des biens immobiliers. Ce chargement ne se fera qu'une fois. Cela prendra 1 minute au plus..."
      });
      this.loader.present();

      setTimeout(() => {
        //après 4m, si il y avais des produits stocker, alors on les use et on toast sinon on demande connection
        this.storage.get('produits').then(result => {
          if (!result) {

            //Si on n'avais jamais recuperer les produits, on alert et on fait retour
            let alertDecon = this.alertCtrl.create({
              title: "Soucis de Connexion",
              subTitle: "Votre connexion est faible, le chargement des produits n'a pas pu se faire",
              buttons: [{
              text: 'Retour',
                handler: () => {
                //TODO quand on clique une notif LOCAL
                  this.navCtrl.setRoot(HomePage);
                }
              }]
            });
            alertDecon.present();

          } else {

            // //Si les produits avait déja été récupéré from API et set alors je toast et je les utilise
            // let toastProduits = this.toastCtrl.create({
            //   message: 'Echec de mise à jour. Utilisation des produits précedemment chargés',
            //   duration: 2000,
            //   position: 'bottom'
            // });
            // toastProduits.present();
            this.storage.get('produits').then(data => { this.tabProduits = data});// Le tableau de produits précédemment stocker est recuperer
          
          }
        })
        .catch(err => {
          console.log('Your data don\'t exist and returns error in catch: ' + JSON.stringify(err));
        });

        this.loader.dismiss();      //On enlève le loading    

      }, 240000); // 4m

    }

    //Methode adaptation des indice de type de produits NUMBER en STRING. Seulement pour la notification de cette page.
      typeNumberToString(num: number): String
      {
        switch (num) {
          case 1 : {
            return "F1";
          }
          case 2 :
              return "F2";
          case 3:
              return "F3";
          case 4:
              return "F4";
          case 5:
              return "F5";
          case 8:
              return "VILLA";
          case 9:
              return "DUPLEX";
          case 10:
              return "MAGASIN";
          default:
              return "inconnu";
        }
      }
    // FIn methode NUMBER to STRING

}
