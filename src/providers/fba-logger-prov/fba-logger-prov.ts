// Provider qui save les logs sur fire analytics
import { Injectable } from '@angular/core';
// Native
import { FirebaseAnalytics } from '@ionic-native/firebase-analytics';

/*
  Generated class for the FbaLoggerProvProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class FbaLoggerProvProvider {

  constructor(public fba: FirebaseAnalytics) {
    console.log('Hello FbaLoggerProvProvider Provider');
  }

  logButton(name:string,value:any){
    this.fba.logEvent(name, { pram:value })
    // .then((res: any) => {console.log(res);})
    // .catch((error: any) => console.error(error));
  }

}
